# README #

This repository contains material for the Git + Docker tutorial at the JGI, December 2016

Download this repository locally from *https://bitbucket.org/TWildish/git-docker-tutorial/get/master.zip*, and unpack it.

## Prerequisites ##
There are some things you need to do *before* coming to the tutorial, in order to have your environment set up correctly. You need to install Git and Docker on your machine, and you need to create and configure an account on Bitbucket.org.

If you've already done all this, great!

### Install Git on your machine ###
You could just use Git from the NERSC login nodes, but they don't have Docker installed, so it will be harder for you to do the remainder of the tutorial.

To install Git, go to *https://git-scm.com/downloads*, and follow the instructions for your platform. If you can run
```
git --version
```
and see something sensible, you're there.

### Install Docker on your machine ###
go to *https://www.docker.com/products/docker* and follow the instructions for your platform. Again, if you can run
```
docker --version
```
then you're done!

### Create/configure a bitbucket account ###
Go to *https://bitbucket.org* and sign up for an account. If you have a google account that you use for work, you can just use that.

Once you're logged in, go to your account settings and configure an SSH key so you can avoid having to type a username and password every time you issue a git command for one of your repositories.

### Now start the tutorial ###
Then go into the **git** directory, then the **docker** directory and follow the instructions there.

You can follow the **docker** tutorial without doing the **git** tutorial, they're independent.