#!/bin/bash

set -e
OUTPUT_DIR=/bbx/output_data
READS=/bbx/input_data/reads.fq.gz

velveth ${OUTPUT_DIR} 31 -fastq.gz ${READS}
velvetg ${OUTPUT_DIR} -cov_cutoff auto

